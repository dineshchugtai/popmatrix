from subprocess import Popen, PIPE
from collections import defaultdict

ONEKG_ADDRESS_VCF = "ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr{chromosome}.phase3_shapeit2_mvncall_integrated_v2.20130502.genotypes.vcf.gz"
ONEKG_PANEL_PATH = "external_files/1kg/integrated_call_samples.20130502.ALL.panel"
SNP_FILE = "example_files/formatted_nature_schizo.tsv"

VCF_FILE_ALL_POPS = "snp.vcf"
POP_LISTS_EXTENSION = ".pop"
CURRENT_WORKING_DIRECTORY = "temp_files/"
FREQUENCY_FILE_EXTENSION = '.frq'

skip_header_line = next

def _return_population_lists(onekg_panel_path):

    super_pop_lists = defaultdict(list)

    with open(onekg_panel_path) as onekg_panel_handle:
        skip_header_line(onekg_panel_handle)

        for line in onekg_panel_handle:
            id, _, super_pop, _ = line.split()
            super_pop_lists[super_pop].append(id)

    return super_pop_lists

def _write_population_lists_to_file(super_pop_lists):

    for pop, id_list in super_pop_lists.items():
        with open(CURRENT_WORKING_DIRECTORY+pop+POP_LISTS_EXTENSION, 'w+') as pop_file:
            for id in id_list:
                pop_file.write(id+'\n')

def _return_snp_dict(snp_file):

    snp_dict = dict()

    with open(snp_file) as snp_file_handle:
        for line in snp_file_handle:
            snp_name, chromosome, nucleotide_position, trait_increasing_allele, allele_regular, p_value = line.split()
            snp_dict[snp_name] = chromosome, nucleotide_position, trait_increasing_allele, allele_regular, p_value

    return snp_dict

def _make_vcf_of_snps(snp_dict, onekg_vcf_address=ONEKG_ADDRESS_VCF):
    

    chromosome_snp_index_string = " {chromosome}:{nucleotide_pos}-{nucleotide_pos}"
    output_directions = ' {redirect} {output_file}'
    tabix_command_general = 'tabix -f {options} ' + onekg_vcf_address + chromosome_snp_index_string + output_directions

    first_snp, get_header, no_header, make_new, redirect = 0, '-h', '', '> ', '>> '
    for snp_number, snp_name in enumerate(snp_dict):
        chromosome, nucleotide_position, trait_increasing_allele, allele_regular, p_value = snp_dict[snp_name]

        if snp_number == first_snp:
            tabix_command_specific = tabix_command_general.format(options=get_header, chromosome=chromosome, nucleotide_pos=nucleotide_position, redirect=make_new, output_file=VCF_FILE_ALL_POPS)
        else:
            tabix_command_specific = tabix_command_general.format(options=no_header, chromosome=chromosome, nucleotide_pos=nucleotide_position, redirect=redirect, output_file=VCF_FILE_ALL_POPS)

        proc = Popen(tabix_command_specific, shell=True, stdout=PIPE, cwd='temp_files')
        for line in proc.stdout:
            print line 

"./vcftools --vcf genotypes.vcf --freq --out allelefrequencies"

def _make_population_vcfs(super_pop_lists):
    
    extract_population_command = 'vcftools --vcf {} --keep {} --recode --stdout > {}'
    
    for pop in super_pop_lists:
        pop_panel = pop+POP_LISTS_EXTENSION
        pop_vcf = pop+".vcf"

        expc_specific = extract_population_command.format(VCF_FILE_ALL_POPS, pop_panel, pop_vcf)
        proc = Popen(expc_specific, shell=True, stdout=PIPE, stderr=PIPE, cwd='temp_files')
        for line in proc.stdout:
            print line 
        for line in proc.stderr:
            print line 

def _compute_allele_frequencies(population_lists):

    compute_allele_frequencies = 'vcftools --vcf {} --freq --out {}'

    for pop in population_lists:
        
        caf_specific = compute_allele_frequencies.format(pop+".vcf", pop)
        proc = Popen(caf_specific, shell=True, stdout=PIPE, stderr=PIPE, cwd=CURRENT_WORKING_DIRECTORY)
    
        for line in proc.stdout:
            print line 
        for line in proc.stderr:
            print line 

#def _convert_frequency_files_to_matrixes(frq_files):
    

def make_matrix():

    population_lists = _return_population_lists(ONEKG_PANEL_PATH)
    _write_population_lists_to_file(population_lists)
    snp_dict = _return_snp_dict(SNP_FILE)
    _make_vcf_of_snps(snp_dict)
    _make_population_vcfs(population_lists)
    _compute_allele_frequencies(population_lists)
    #_convert_frequency_files_to_matrixes()

make_matrix()
